jQuery(document).ready(function($) {
	
	$(".front-slidedown").click(function() {
		document.getElementById("landingmenu").scrollIntoView();  
	});

	// slider infografias
	var mySwiper = new Swiper ('.swiper-container', {
	    direction: 'horizontal',
	    loop: true,

	    pagination: {
	      el: '.swiper-pagination',
	      clickable: true,
	    },

	    navigation: {
	      nextEl: '.swiper-button-next-custom',
	      prevEl: '.swiper-button-prev-custom',
	    }
	});

    // slider tipologías
    var typologySwiper = new Swiper ('.typology-swiper-container', {
        direction: 'horizontal',
        loop: true,

        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },

        navigation: {
          nextEl: '.swiper-button-next-custom',
          prevEl: '.swiper-button-prev-custom',
        }
    });

	// plotly infografias
	plotChart("any", "scatter", "crímenes de odio por año", "plotYear", true);
	plotChart("sentencia", "bar", "crímenes de odio por sentencia", "plotSentencia");
	plotChart("delito", "bar", "crímenes de odio por delito", "plotDelito");
});

function plotChart(dataType, chartType, title, htmlElement, xaxis=false) {

    var colors = ['#aa0000', '#000000', '#00aa00', '#0000aa'];

    // define width and height
    var w = 700,
        h = 450,
        t = 100;
    if (window.mobilecheck()) {
        w = 350;
        h = 300;
        t = 0;
    }

    Plotly.d3.tsv("https://crimenesdeodio.info/"+getLang()+"/data/?t="+dataType, function(err, rows) {

    	var data = [],
            keys = Object.keys(rows[0]),
            text = unpackCombine(rows, keys[1], keys[0]).map(String),
            hoverinfo = 'none';

        if (dataType === "any") {
        	text = unpack(rows, keys[0]).map(String);
        	hoverinfo = 'auto';
        }

        var max = 0;

        for (var i=1; i<keys.length; i++) {

            var yValues = unpack(rows, keys[i]);
            thisMax = Math.max.apply(null, yValues);
            if (thisMax > max) max = thisMax;
            
            data.push({
                type: chartType,
                textposition: 'outside',
                x: unpack(rows, keys[0]),
                y: yValues,
                text: text,
                textfont: {
                    size: 20,
                    color: '#000000',
                },
                hoverinfo: hoverinfo,
                //xaxis: 'x1',
                yaxis: 'y1',
                name: keys[i],
                marker: {
                    //color: colors,
                    color: '#e01a4d',
                }
            });
        }

        if (dataType !== "any") max *= 1.4;

        var layout = {
            width: w,
            height: h,
            xaxis: {
                visible: xaxis,
                tickangle: -25,
                fixedrange: true,
            },
            yaxis: {
                range: [0,max*1.2],
                /*title: {
                    text: 'cantidad',
                },*/
                //hoverformat: ".1f",
                fixedrange: true,
            },
            legend: {
                //x: 1,
                y: 0.5,
                itemclick: false,
                itemdoubleclick: false,
            },
            margin: {
                t: t,
                b: 60,
            }
        }

        var options = {
            displayModeBar: false,
            showLink: false,
            locale: 'es'
        }

    	var plotDiv = document.getElementsByClassName(htmlElement);
        Plotly.newPlot(plotDiv[0], data, layout, options);
        
       	// fix for swiper duplicate slides (first and last)
        if (plotDiv.length > 1) {
	        Plotly.newPlot(plotDiv[1], data, layout, options);
        }
    });
}

// convert array of objects to array of elements defined by key
function unpack(rows, key) {
    var unpack = rows.map(function(row) { return row[key]; });
    // remove last element in case last line is empty
    if (unpack[unpack.length-1] === undefined) 
        unpack = unpack.slice(0,unpack.length-1);
    return unpack;
}

// convert array of objects to array of elements defined by 2 keys
function unpackCombine(rows, key1, key2) {
    var unpack = rows.map(function(row) { return row[key1]+"<br>"+row[key2]; });
    // remove last element in case last line is empty
    if (unpack[unpack.length-1] === undefined) 
        unpack = unpack.slice(0,unpack.length-1);
    return unpack;
}

window.mobilecheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

//get language from URL
function getLang() {
    var language = "ca";
    var loc = window.location.href;
    var url = "crimenesdeodio.info/"
    var pos1 = loc.indexOf(url);
    loc = loc.substring(pos1+url.length);
    loc = loc.split("/");
    if (loc[0] == "en" || loc[0] == "es") {
        language = loc[0];
    }
    return language;
}