<?php
/**
 * The Template for displaying hatecrime single posts as images.
 *
 * @package Cryout Creations
 * @subpackage nirvana
 * @since nirvana 0.5
 */

//get_header(); ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="main" class="single-fitxa">

	<section id="container" class="<?php echo nirvana_get_layout_class(); ?>">
	
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<?php 
			$terms = get_the_terms($post->ID, "type");

			$bgimg = "";
			if ( has_post_thumbnail( get_the_ID() ) ) {
				$bgimg = "style='background-image:url(" . get_the_post_thumbnail_url( get_the_ID(), 'large' ) . ")'";
			}
			else {
				$img = "";
				if (count($terms) == 1) {
					$img = $terms[0]->slug . rand(1,6);
				}
				else {
					foreach ($terms as $i => $term) {
						if ($i > 0) $img .= "_";
						$img .= $term->slug;
					}
				}
				$bgimg = "style='background-image:url(" . get_stylesheet_directory_uri() . "/bgfichas/" . $img . ".jpg)'";
			}
		?>

		<div id="content" role="main">

			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php echo $bgimg; ?>>

				<div class="entry-icons">
					<?php foreach ($terms as $term) { ?>
					<?php if (function_exists('get_wp_term_image')) { echo "<img src='".get_wp_term_image($term->term_id)."'/>"; } ?>
					<?php } ?>
				</div>

				<div class="entry-category">
					<h2>
						<?php 
							foreach ($terms as $i => $term) {
								if ($i > 0) echo ", ";
								echo wpm_translate_string($term->name);
							}
						?>
					</h2>
				</div>

				<div class="entry-date">
					<h3>
					<?php 
						$date = strtotime(get_post_meta($post->ID, "date", true));
						if (wpm_get_language() == "ca") {
							setlocale(LC_TIME, "ca_ES");
						}
						else if (wpm_get_language() == "es") {
							setlocale(LC_TIME, "es_ES");
						}
						else if (wpm_get_language() == "en") {
							setlocale(LC_TIME, "en_US");
						}
						echo strftime("%d/%m/%Y", $date) . ". ".get_post_meta($post->ID, "city", true);
					?>
					</h3>
				</div>

				<div class="entry-header">
					<h1 class="title">
						<?php 
							$name = explode(" ", get_the_title());
							$nameArr = $name;
							$firstname = $name[0];
							$lastname = implode(" ", array_splice($name, 1));
							if (count($nameArr) > 5) {
								$firstname = $nameArr[0]." ".$nameArr[1]." ".$nameArr[2];
								$lastname = implode(" ", array_splice($nameArr, 3));
							}
							else if (count($nameArr) > 3) {
								$firstname = $nameArr[0]." ".$nameArr[1];
								$lastname = implode(" ", array_splice($nameArr, 2));
							}
							echo $firstname." <br>".$lastname;
						?>
					</h1>
				</div>

				<div class="entry-content">
					<strong><?php _e("Delict", "hatecrimes")?>: </strong>
					<?php 
						$delictterms = get_the_terms($post->ID, "delict");
						$out = "";
						if ($delictterms) {
							foreach ($delictterms as $i => $term) {
								if ($i > 0) $out .= ", ";
								$out .= wpm_translate_string($term->name);
							}
						}
						if ($out !== "") echo $out;
						else _e("unknown", "hatecrimes");
					?>
					
					<span>//</span>

					<strong><?php _e("Sentence", "hatecrimes")?>: </strong>
					<?php 
						$sentenceterms = get_the_terms($post->ID, "sentence_type");
						$out = "";
						if ($sentenceterms) {
							foreach ($sentenceterms as $i => $term) {
								if ($i > 0) $out .= ", ";
								$out .= wpm_translate_string($term->name);
							}
						}
						if ($out !== "") echo $out;
						else _e("unknown", "hatecrimes");
					?>						
					<?php 
						$out = get_post_meta($post->ID, "sentence", true); 
						if ($out !== "" && strpos("NO", $out)) echo "<p>".wpm_translate_string($out)."</p>";
					?>
				</div>

				<div class="entry-info">
					<div class="left"><?php _e("More information at", "hatecrimes")?> <strong>crimenesdeodio</strong>.info</div>

					<div class="right">
						<a href="<?php echo get_site_url(); ?>"><span class="logo"></span></a>
						<a class="icon" href="https://twitter.com/crimenesdeodio"><span class="twitter"></span></a><a class="icon" href="https://www.facebook.com/Cr%C3%ADmenes-de-Odio-1645711305670191/"><span class="facebook"></span></a>
					</div>
				</div>

			</div><!-- #post-## -->

		</div><!-- #content -->

		<?php endwhile; // end of the loop. ?>
		
	</section><!-- #container -->

	</div><!-- #main -->

<?php wp_enqueue_script('html2canvas-js', get_stylesheet_directory_uri() . "/html2canvas.min.js"); ?>

<?php wp_footer(); ?>

<script>
	jQuery(function() {
		html2canvas(document.querySelector("#content"))
		.then(function (canvas) {
			console.log(canvas.toDataURL("image/png"));
	        let filename = jQuery(".title").text().trim()+'.png';
		    jQuery.post("/ca/save-fitxa-image?filename=" + filename, {
		    	data: canvas.toDataURL("image/png")
		    }, function (file) {
	        	//window.location.href = "download.php?path=" + file
	        	console.log("https://crimenesdeodio.info/wp-content/hatecrimes-images/"+encodeURI(JSON.parse(file)));
	        });
	    });
	});
</script>

</body>
</html>
