<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package Cryout Creations
 * @subpackage nirvana
 * @since nirvana 0.5
 */
?>	<div style="clear:both;"></div>
	</div> <!-- #forbottom -->

	<footer id="myfooter" role="contentinfo">

		<div class="left">
			<a href="<?php echo get_site_url(); ?>"><span class="logo"></span></a><!--<strong>crímenes de <span class="strike">odio</span></strong>-->
		</div>
		<div class="right">
			<a class="icon" href="https://twitter.com/crimenesdeodio"><span class="twitter"></span></a><a class="icon" href="https://www.facebook.com/Cr%C3%ADmenes-de-Odio-1645711305670191/"><span class="facebook"></span></a>
		</div>
		<div class="middle">
			<?php _e("With the collaboration of", "hatecrimes")?> <img width="200" src="<?php echo get_stylesheet_directory_uri() ?>/assets/SOS RACISME.svg" />
		</div>

	</footer><!-- #footer -->

	</div><!-- #main -->
</div><!-- #wrapper -->


<?php	wp_footer(); ?>

</body>
</html>
