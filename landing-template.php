<?php
/**
 * Template Name: Landing
 *
 * @package Cryout Creations
 * @subpackage nirvana
 * @since nirvana 0.5
 */

// manual aniversary for test purposes
$aniversari = filter_input(INPUT_GET, 'aniversari', FILTER_SANITIZE_STRING);

$args = array(
    'post_type' => 'hatecrime',
    'posts_per_page' => 1,
);

$meta = "";
if ($aniversari) {
	// aniversari MANUAL
	$args['name'] = $aniversari;
}
else {
	// aniversari for TODAY
	$args['meta_query'] = array(
		'relation' => 'AND',
        array(
            'key'   => 'date',
            'value' => date('m/d/'),
            'compare' => 'LIKE'
        )
	);
	$aniversari = true;
}

if ($aniversari) {

	$hatecrime = get_posts($args);

	if (count($hatecrime) > 0) {
		$hatecrime = $hatecrime[0];
	}
	else {
		$aniversari = false;
	}
}
else {
	$aniversari = false;
}

// get category image
$terms = get_the_terms($hatecrime->ID, "type");

$bgimg = "style='background-image:url(";
if (!$aniversari) {
	$bgimg .= get_stylesheet_directory_uri() . "/bgfichas/landing" . rand(1,5) . ".png)";
}
else {
	if ( has_post_thumbnail( $hatecrime->ID ) ) {
		$bgimg .= get_the_post_thumbnail_url( $hatecrime->ID, 'large' ) . ")";
	}
	else {
		$img = "";
		if (count($terms) == 1) {
			$img = $terms[0]->slug . rand(1,6);
		}
		else {
			foreach ($terms as $i => $term) {
				if ($i > 0) $img .= "_";
				$img .= $term->slug;
			}
		}
		$bgimg .= get_stylesheet_directory_uri() . "/bgfichas/" . $img . ".jpg)";
	}
}
$bgimg .= "; background-size:cover;'";

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php  cryout_meta_hook(); ?>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php
	 	cryout_header_hook();
		wp_head(); ?>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/landing/landing.css" />
	<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
</head>
<body <?php body_class(); ?>>

<section id="container" class="front <?php echo nirvana_get_layout_class(); ?>">
	<div id="content" role="main">

		<section id="landing1" class="landing active" <?php echo $bgimg; ?>>

			<div class="front-header">
				<?php
					if ( function_exists ( 'wpm_language_switcher' ) ) wpm_language_switcher ('list', 'name');
				?>

				<div class="right"><span class="logo"></span></div>
			</div>

			<!--<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="entry-content">
					<?php the_content(); ?>
				</div>

			</div>
			<?php endwhile; // end of the loop. ?>-->

			<div class="center">

				<?php if ($aniversari) { ?>

					<div class="aniversari">

					<?php 
						foreach ($terms as $term) {
							if (function_exists('get_wp_term_image')) { echo "<img src='".get_wp_term_image($term->term_id)."'/>"; 
							}
						} ?>

    					<h1><?php echo wpm_translate_string($hatecrime->post_title); ?></h1>

						<h3>
						<?php 
							$date = strtotime(get_post_meta($hatecrime->ID, "date", true));
							setlocale(LC_TIME, "es");
							echo strftime("%d/%m/%Y", $date) . ". ".get_post_meta($hatecrime->ID, "city", true);
						?>
						</h3>

						<p><?php echo mb_strimwidth(wpm_translate_string($hatecrime->post_content), 0, 400, '...'); ?></p>

						<p><a href="<?php echo get_permalink($hatecrime->ID); ?>" class="landing-btn"><?php _e("More information", "hatecrimes")?></a></p>

					</div>

    			<?php } else { ?>

					<h1><img class="fronticon" src="<?php echo get_stylesheet_directory_uri()?>/assets/logo_white.svg" alt="logo" />crimenesdeodio.info</h1>

					<?php if (wpm_get_language() == "ca"): ?>

						<p>D’acord amb la definició de l’Organització per a la Seguretat i la Cooperació a Europa (OSCE), un crim d’odi és <i>“tota infracció penal, incloses les infraccions contra les persones i la propietat, quan la víctima, el lloc o l’objecte de la infracció són seleccionats a causa de la seva connexió, relació, afiliació, suport o pertinença real o suposada a un grup que pugui estar basat en la “raça”, origen nacional o ètnic, l’idioma, el color, la religió, l’edat, la minusvalidesa física o mental, l’orientació sexual o altres factors similars, siguin reals o suposats”</i>.</p>

					<?php elseif (wpm_get_language() == "es"): ?>

						<p>De acuerdo con la definición de la Organización para la Seguridad y la Cooperación en Europa (OSCE), un crimen de odio es <i>"toda infracción penal, incluidas las infracciones contra las personas y la propiedad, cuando la víctima, el lugar o el objeto de la infracción son seleccionados debido a su conexión, relación, afiliación, apoyo o pertenencia real o supuesta a un grupo que pueda estar basado en la "raza", origen nacional o étnico, el idioma, el color, la religión, la edad, la minusvalía física o mental, la orientación sexual u otros factores similares, ya sean reales o supuestos"</i>.</p>

					<?php endif; ?>

				<?php } ?>

				<div class="front-slidedown"></div>

			</div>

			<div class="front-footer footer">
				<div class="left">
					<strong>memoria contra el <span class="strike1"><span class="strike2">olvido</span></span></strong>
				</div>
				<div class="right">
					<a class="icon" href="https://twitter.com/crimenesdeodio"><span class="twitter"></span></a><a class="icon" href="https://www.facebook.com/Cr%C3%ADmenes-de-Odio-1645711305670191/"><span class="facebook"></span></a>
				</div>
			</div>

		</section>

		<section id="landingmenu" class="landing">
			<a id="nav-toggle"><span>&nbsp;</span></a>
			<nav id="access" role="navigation">
				<?php cryout_access_hook();?>
				<?php if ( function_exists ( 'wpm_language_switcher' ) ) wpm_language_switcher ('list', 'name'); ?>
			</nav><!-- #access -->
		</section>

		<?php
			$args = array(
			    'post_type' => 'hatecrime',
			);
			$my_query = new WP_Query($args);
			$total = $my_query->found_posts;
		?>

		<section id="landing2" class="landing">
			<div class="col_left">
				<?php if (wpm_get_language() == "ca"): ?><h1>El projecte</h1><p>Aquest projecte és una eina de memòria sobre els <strong>crims d’odi amb resultat de mort comesos a l’Estat espanyol entre 1990 i 2020</strong>. Hi trobaràs una base de dades que recull els <?php echo $total?> casos amb <?php echo (int)$total+2?> víctimes que hem pogut identificar, contrastar, classificar per tipologia i documentar, amb la intenció de recuperar la memòria de les víctimes mortals de l'odi.</p><p>* Alguns casos han estat classificats en més d'una tipologia, per aquest motiu la suma de les xifres d'aquest gràfic és superior a la del total de casos recollits.</p><p class="foot"><a href="/<?php echo wpm_get_language(); ?>/casos/" class="landing-btn">Ves a casos</a></p><?php elseif (wpm_get_language() == "es"): ?><h1>El proyecto</h1><p>Este proyecto es una herramienta para la memoria de los <strong>crímenes de odio con resultado de muerte cometidos en el Estado español entre 1990 y 2020</strong>. Encontrarás una base de datos que recoge los <?php echo $total?> casos con <?php echo (int)$total+2?> víctimas que hemos podido identificar, contrastar, clasificar por tipología y documentar con la intención de recuperar la memoria de las víctimas mortales del odio.</p><p>* Algunos casos han sido clasificados en más de una tipología, por ese motivo la suma de las cifras de este gráfico es superior a la del total de casos recogidos.</p><p class="foot"><a href="/<?php echo wpm_get_language(); ?>/casos/" class="landing-btn">Visita los casos</a></p><?php endif; ?>
			</div>

			<div class="col_right">
				<?php
					echo "<h1>" . $total . "</h1><h3>casos</h3>";

					$terms = get_terms(array('type'));

					foreach ($terms as $term) {
						if ($term->slug == 'otros') {
							$otros = $term;
						}
						else {
							$args = array(
							    'post_type' => 'hatecrime',
								'tax_query' => array(
									array(
										'taxonomy' => 'type',
										'field'    => 'slug',
										'terms'    => $term->slug,
									),
								),
							);
							$my_query = new WP_Query($args);
							echo "<div class='type " . $term->slug . "'>" . $term->name . " <span class='num'>" . $my_query->found_posts . "</span><span class='bar' style='width:" . ($my_query->found_posts*5+5) . "px'></span><img class='svgimg' src='" . get_stylesheet_directory_uri() . "/landing/marker-" . $term->slug . ".svg'/></div>";
						}
					}

					// otros como último
					$term = $otros;
					$args = array(
					    'post_type' => 'hatecrime',
						'tax_query' => array(
							array(
								'taxonomy' => 'type',
								'field'    => 'slug',
								'terms'    => $term->slug,
							),
						),
					);
					$my_query = new WP_Query($args);
					echo "<div class='type " . $term->slug . "'>" . $term->name . " <span class='num'>" . $my_query->found_posts . "</span><span class='bar' style='width:" . ($my_query->found_posts*5+5) . "px'></span><img class='svgimg' src='" . get_stylesheet_directory_uri() . "/landing/marker-" . $term->slug . ".svg'/></div>";
				?>
			</div>
		</section>

		<section id="landing3" class="landing">
			<div class="col_left">
				<h1>El mapa</h1>

				<?php if (wpm_get_language() == "ca"): ?>

					<p>El mapa interactiu et permetrà navegar per la geografia de l'Estat espanyol per tal de poder cercar i consultar els diferents casos que hem pogut documentar en aquest projecte. Pots accedir a la fitxa de cada cas, classificada per data, lloc del crim, tipologia, sentència o delicte, accedir a una breu explicació dels fets, així com consultar els diferents materials que hem fet servir com a fonts.</p>

				<?php elseif (wpm_get_language() == "es"): ?>

					<p>El mapa interactivo te permitirá navegar por la geografía del Estado español para poder buscar y consultar los diferentes casos que hemos podido documentar en este proyecto. Puedes acceder a la ficha de cada caso, clasificada por fecha, lugar del crimen, tipología, sentencia o delito, acceder a una breve explicación de los hechos, así como consultar los diferentes materiales que hemos utilizado como fuentes.</p>

				<?php endif; ?>

				<p class="foot"><a href="/<?php echo wpm_get_language(); ?>/map/" class="landing-btn">Visita el mapa</a></p>
				
			</div>

			<div class="col_right">
				<a href="/<?php echo wpm_get_language(); ?>/map/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/landing/map.png"/></a>
			</div>
		</section>

		<section id="landing4" class="landing">

			<div class="swiper-container">
			    <div class="swiper-wrapper">
			        <div class="swiper-slide">
			        	<div class="col_left">
			        		<?php if (wpm_get_language() == "ca"): ?>
					        	<h1>Crims d'odi per any</h1>
					        	<p>La següent gràfica mostra l'evolució anual dels crims d'odi, des de 1990 fins 2020.</p>
							<?php elseif (wpm_get_language() == "es"): ?>
					        	<h1>Crímenes de odio por año</h1>
					        	<p>El siguiente gráfico muestra la evolución anual de los crímenes de odio, desde 1990 hasta 2020.</p>
							<?php endif; ?>
				        </div>
						<div class="col_right">
							<div class="plotYear"></div>
						</div>
			        </div>
			        <div class="swiper-slide">
			        	<div class="col_left">
			        		<?php if (wpm_get_language() == "ca"): ?>
					        	<h1>Crims d'odi per sentència</h1>
					        	<p>La següent gràfica mostra els crims d'odi recollits en aquest projecte que han arribat a judici, tant els que han conclòs amb condemna com els sentenciats amb l'absolució de les persones acusades. També mostra la quantitat de casos arxivats que no han estat jutjats i aquells dels quals ni tan sols s’ha pogut obtenir la informació.</p>
							<?php elseif (wpm_get_language() == "es"): ?>
					        	<h1>Crímenes de odio por sentencia</h1>
					        	<p>El siguiente gráfico muestra los crímenes de odio recogidos en este proyecto que han sido juzgados, tanto los que han concluido con en condena como los sentenciados con la absolución de las personas acusadas. También muestra la cantidad de casos archivados que no han sido juzgados y aquellos de los que ni tan siquiera se ha podido obtener la información.</p>
							<?php endif; ?>
				        </div>
						<div class="col_right">
							<div class="plotSentencia"></div>
						</div>
			        </div>
			        <div class="swiper-slide">
			        	<div class="col_left">
			        		<?php if (wpm_get_language() == "ca"): ?>
					        	<h1>Crims d'odi per delicte</h1>
					        	<p>La següent gràfica distingeix els crims d'odi recollits en aquest projecte que han estat jutjats, classificant-los segons hagin estat sentenciats com a assassinat, homicidi o altres delictes. La principal diferència entre una sentència per assassinat o per homicidi és que en el primer cas es demostra que hi ha hagut premeditació o intenció de matar la víctima.</p>
							<?php elseif (wpm_get_language() == "es"): ?>
					        	<h1>Crímenes de odio por delito</h1>
					        	<p>El siguiente gráfico distingue los crímenes de odio recogidos en este proyecto que han sido juzgados, clasificándolos según hayan sido sentenciados como asesinato, homicidio u otros delitos. La principal diferencia entre una sentencia por asesinato o por homicidio es que en el primer caso se demuestra que ha habido premeditación o intención de matar a la víctima.</p>
							<?php endif; ?>
				        </div>
						<div class="col_right">
							<div class="plotDelito"></div>
						</div>
			        </div>
			    </div>
			    <div class="swiper-pagination"></div>
			    <div class="swiper-button-prev-custom"></div>
			    <div class="swiper-button-next-custom"></div>
			</div>
		</section>

		<section id="landing5" class="landing">
			<h1>Tipologías</h1>

			<table class="tipo">
				<tr>

				<?php 
				
				$terms = get_terms('type', array('hide_empty'=>false));
	                         
				if ( $terms && ! is_wp_error( $terms ) ) {
				 
				    $types = array();

				    $i = 0;
				 
				    foreach ( $terms as $term ) {

				    	if ($term->slug == "otros") {
				    		$otros = $term;
				    	}
				    	else {

					    	if (($i-1)%4 == 0) echo "<tr>";
					        ?>

					        <td>
								<div class="clip"><img src="<?php if (function_exists('get_wp_term_image')) { echo get_wp_term_image($term->term_id); } ?>"/></div>
								<h3><span class="strike1"><span class="strike2"><?php echo $term->name; ?></span></span></h3>
								<p><?php echo $term->description; ?></p>

								<?php if ($term->slug != "misoginia") : ?>

								<a href="/<?php echo wpm_get_language(); ?>/casos/?type=<?php echo $term->name; ?>" class="landing-btn">Casos d<?php $first = strtolower($term->name[0]); if (wpm_get_language() == "ca" && ($first == 'a' || $first == 'e' || $first == 'i' || $first == 'o' || $first == 'u')) echo "'"; else echo "e "; ?><?php echo $term->name; ?></a>

								<?php else : ?>

								<a target="_blank" href="https://feminicidio.net/" class="landing-btn black">feminicidio.net</a>

								<?php endif; ?>

							</td>

					        <?php

					    	if ($i%4 == 0) echo "</tr>";
					    }

				    	$i++;
				    }

				    // otros como último
				    $term = $otros;
				    ?>

				    <td>
						<div class="clip"><img src="<?php if (function_exists('get_wp_term_image')) { echo get_wp_term_image($term->term_id); } ?>"/></div>
						<h3><span class="strike1"><span class="strike2"><?php echo $term->name; ?></span></span></h3>
						<p><?php echo $term->description; ?></p>

						<a href="/<?php echo wpm_get_language(); ?>/casos/?type=<?php echo $term->name; ?>" class="landing-btn">Casos d'<?php echo $term->name; ?></a>
					</td>
				
				<?php } ?>
				</tr>

			</table>
			<?php if (wpm_get_language() == "ca"): ?>
				<div class="feminicidio">* Els crims d’odi per misogínia no estan incorporats en aquesta investigació perquè ja hi ha un registre d’aquest tipus de crims a la pàgina web <a target="_blank" href="https://feminicidio.net">www.feminicidio.net</a></div>
			<?php elseif (wpm_get_language() == "es"): ?>
				<div class="feminicidio">* Los crímenes de odio por misoginia no han sido incorporados en esta investigación porque ya existe un registro de este tipo de crímenes en la página web <a target="_blank" href="https://feminicidio.net">www.feminicidio.net</a></div>
			<?php endif; ?>

			<div id="typology" class="swiper-container">
			    <div class="swiper-wrapper">

				<?php 
				
				$terms = get_terms('type', array('hide_empty'=>false));
	                         
				if ( $terms && ! is_wp_error( $terms ) ) {
				 
				    $types = array();
				 
				    foreach ( $terms as $term ) : ?>

				    	<?php if ($term->slug == 'otros') {
							$otros = $term;
						}
						else { ?>

				        <div class="swiper-slide">

							<div class="clip"><img src="<?php if (function_exists('get_wp_term_image')) { echo get_wp_term_image($term->term_id); } ?>"/></div>
							<h3><span class="strike1"><span class="strike2"><?php echo $term->name; ?></span></span></h3>
							<p><?php echo $term->description; ?></p>

							<?php if ($term->slug != "misoginia") : ?>

								<a href="/<?php echo wpm_get_language(); ?>/casos/?type=<?php echo $term->name; ?>" class="landing-btn">Casos de <?php echo $term->name; ?></a>

							<?php else : ?>

								<a target="_blank" href="https://feminicidio.net/" class="landing-btn black">feminicidio.net</a>

								<?php if (wpm_get_language() == "ca"): ?>
									<p>* Els crims d’odi per misogínia no estan incorporats en aquesta investigació perquè ja hi ha un registre d’aquest tipus de crims a la pàgina web <a target="_blank" href="https://feminicidio.net">www.feminicidio.net</a></p>
								<?php elseif (wpm_get_language() == "es"): ?>
									<p>* Los crímenes de odio por misoginia no han sido incorporados en esta investigación porque ya existe un registro de este tipo de crímenes en la página web <a target="_blank" href="https://feminicidio.net">www.feminicidio.net</a></p>
								<?php endif; ?>

							<?php endif; ?>

				        </div>

				    <?php } endforeach;

				    // otros como último
				    $term = $otros;
				    ?>

				   	<div class="swiper-slide">

						<div class="clip"><img src="<?php if (function_exists('get_wp_term_image')) { echo get_wp_term_image($term->term_id); } ?>"/></div>
						<h3><span class="strike1"><span class="strike2"><?php echo $term->name; ?></span></span></h3>
						<p><?php echo $term->description; ?></p>

						<?php if ($term->slug != "misoginia") : ?>

							<a href="/<?php echo wpm_get_language(); ?>/casos/?type=<?php echo $term->name; ?>" class="landing-btn">Casos de <?php echo $term->name; ?></a>

						<?php else : ?>

							<a target="_blank" href="https://feminicidio.net/" class="landing-btn black">feminicidio.net</a>

							<?php if (wpm_get_language() == "ca"): ?>
								<p>* Els crims d’odi per misogínia no estan incorporats en aquesta investigació perquè ja hi ha un registre d’aquest tipus de crims a la pàgina web <a target="_blank" href="https://feminicidio.net">www.feminicidio.net</a></p>
							<?php elseif (wpm_get_language() == "es"): ?>
								<p>* Los crímenes de odio por misoginia no han sido incorporados en esta investigación porque ya existe un registro de este tipo de crímenes en la página web <a target="_blank" href="https://feminicidio.net">www.feminicidio.net</a></p>
							<?php endif; ?>

						<?php endif; ?>

			        </div>

				<?php } ?>

			    </div>
			    <div class="swiper-pagination"></div>
			    <div class="swiper-button-prev-custom"></div>
			    <div class="swiper-button-next-custom"></div>
			</div>
		</section>

		<div class="footer">
			<div class="left">
				<span class="logo"></span><!--<strong>crímenes de odio</strong>-->
			</div>
			<div class="right">
				<a class="icon" href="https://twitter.com/crimenesdeodio"><span class="twitter"></span></a><a class="icon" href="https://www.facebook.com/Cr%C3%ADmenes-de-Odio-1645711305670191/"><span class="facebook"></span></a>
			</div>
			<div class="middle">
				<?php _e("With the collaboration of", "hatecrimes")?> <img width="200" src="<?php echo get_stylesheet_directory_uri() ?>/assets/SOS RACISME.svg" />
			</div>
		</div>

	</div>
</section>

<?php wp_footer(); ?>

<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/landing/landing.js"></script>

</body>
</html>
