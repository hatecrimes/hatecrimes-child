<?php
/**
	Template Name: Map
 */

get_header();?>

	<section id="container">
	    <div id="contentfilter">
	    	<div>
	    		<?php if (wpm_get_language() == "ca"): ?>

	    			<h1>El mapa dels crims d'odi a l'estat espanyol</h1>
	    			<p class="intro">El mapa permet localitzar els diferents casos recollits i classificats per la identitat de la víctima, la data i el lloc on van succeir els fets, la tipologia, la sentència i el delicte.</p>

	    		<?php elseif (wpm_get_language() == "es"): ?>

	    			<h1>El mapa de los crímenes de odio en el estado español</h1>
	    			<p class="intro">El mapa permite localizar los diferentes casos recogidos y clasificados por la identidad de la víctima, la fecha y lugar donde sucedieron los hechos, la tipología, la sentencia y el delito.</p>

	    		<?php endif; ?>
	    	</div>

			<div class="limiter">
				<div id="divfilter">
				  	<form id="filter"></form>
				</div>
			</div>
	    </div>

		<div id="map"></div>
		<!--<div id="map2"></div>-->

		<script src="<?php echo get_site_url(); ?>/wp-content/plugins/hatecrimes-map/lib/leaflet.js"></script>
		<script src="<?php echo get_site_url(); ?>/wp-content/plugins/hatecrimes-map/lib/leaflet.markercluster.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/leaflet-easybutton@2/src/easy-button.js"></script>
		<script src="<?php echo get_site_url(); ?>/wp-content/plugins/hatecrimes-map/map.js"></script>

	</section><!-- #container -->

<?php get_footer(); ?>