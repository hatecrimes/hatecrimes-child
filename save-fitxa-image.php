<?php

/**
 * Template Name: Image download
 * The Template for downloading hatecrime single posts fitxas images.
 */

if (isset($_GET["filename"])) {

    $data = $_POST['data'];
    $file = $_GET['filename'];

    // remove "data:image/png;base64,"
    $uri = substr($data, strpos($data, ",") + 1);

    // save to file
    file_put_contents(WP_CONTENT_DIR . '/hatecrime-images/' . $file, base64_decode($uri));

    // return the filename
    echo json_encode($file);
}
?>