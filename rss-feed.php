<?php
/**
	Template Name: RSS FEED
*/

header("Content-Type: application/rss+xml; charset=UTF-8");
$fp = fopen('php://output', 'w');

$rssfeed = '<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
<channel>
<title>Crímenes de Odio</title>
<link>https://crimenesdeodio.info</link>
<description></description>
<managingEditor>info@crimenesdeodio.info (Info)</managingEditor>
<language>es-es</language>
<image>
    <url>https://crimenesdeodio.info/wp-content/uploads/2024/01/header.jpg</url>
    <title>Crímenes de Odio</title>
    <link>https://crimenesdeodio.info</link>
</image>
<copyright>Copyleft (CC) 2023 crimenesdeodio.info</copyright>'.PHP_EOL;

$my_query = new WP_Query('post_type=hatecrime&posts_per_page=500&order=ASC&orderby=fecha_del_crimen');

if ( have_posts() ) {

    global $post;
    //$custom = get_post_custom( $post->ID );

    //get_field("fecha_del_crimen", $post_id, false);

    while ($my_query->have_posts()) {

        $my_query->the_post();

        $date = get_field("fecha_del_crimen");
        $day = substr($date,0,2);
        $month = substr($date,3,2);
        $year = substr($date,6,4);

        // only get hate crimes which yearly aniversary already has happened
        if (date('m/d/Y') >= date("m/d/Y", mktime(0, 0, 0, $month, $day, date('Y')))) {

            $diff = date('Y')-$year;
            $imgurl = get_site_url() . '/wp-content/hatecrime-images/' . rawurlencode(get_the_title()) . '.png';
            $imgpath = ABSPATH . '/wp-content/hatecrime-images/' . get_the_title() . '.png';

            $rssfeed .= '<item>
    <title>'. $date . ': ' . get_the_title() . '</title>
    <guid>' . get_the_guid() . '</guid>
    <link>'. get_permalink() . '</link>
    <description>#CrimenesDeOdio | Hoy hace ' . $diff . ' años '. get_field("toot") . ' +info 👇' . get_permalink() . '</description>
    <pubDate>' . date('r', mktime(0, 0, 0, $month, $day, date('Y'))) . '</pubDate>
    <enclosure url="' . $imgurl . '" length="' . filesize($imgpath) . '" type="image/png" />
 </item>';
       }
    }
}

$rssfeed .= PHP_EOL.'</channel>
</rss>'.PHP_EOL;

echo $rssfeed;

fclose($fp);

?>