<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Cryout Creations
 * @subpackage nirvana
 * @since nirvana 0.5
 */

get_header();?>

		<section id="container" class="<?php echo nirvana_get_layout_class(); ?>">
			<div id="content" role="main">
				<?php cryout_before_content_hook(); ?>
			
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php 
						$terms = get_the_terms($post->ID, "type");
						foreach ($terms as $term) : ?>

						<div class="cat <?php echo $term->slug; ?>">
							<h3><?php
									echo wpm_translate_string($term->name);
									if (function_exists('get_wp_term_image')) { echo "<img src='".get_wp_term_image($term->term_id)."'/>"; 
									}
								?>
							</h3>
						</div>

					<?php endforeach; ?>

					<h1 class="entry-title">
						<?php the_title(); ?>
					</h1>
					<?php cryout_post_title_hook(); ?>

					<div id="fitxa-description" class="entry-content">
						<h3>
						<?php 
							$date = strtotime(get_post_meta($post->ID, "date", true));
							if (wpm_get_language() == "ca") {
								setlocale(LC_TIME, "ca_ES");
								if (strftime("%B", $date) == "abril" ||
									strftime("%B", $date) == "agost" ||
									strftime("%B", $date) == "octubre") {
									echo utf8_encode(strftime("%e d'%B de %Y", $date));
								}
								else {
									echo utf8_encode(strftime("%e %B de %Y", $date));
								}
							}
							else if (wpm_get_language() == "es") {
								setlocale(LC_TIME, "es_ES");
								echo utf8_encode(strftime("%e de %B de %Y", $date));
							}
							else if (wpm_get_language() == "en") {
								setlocale(LC_TIME, "en_US");
								echo strftime("%B %e, %Y", $date);
							}
							echo ". ".get_post_meta($post->ID, "city", true);
						?>
						</h3>
						<?php the_content(); ?>
					</div><!-- .entry-content -->

					<div id="fitxa" class="entry-content">
						<p><strong><?php _e("Type", "hatecrimes")?>: </strong><br>
						<?php 
							foreach ($terms as $i => $term) {
								if ($i > 0) echo ", ";
								echo wpm_translate_string($term->name);
							}
						?>
						</p>

						<!--<p><strong><?php _e("Judicial body", "hatecrimes")?>: </strong><br>
						<?php 
							$out = get_post_meta($post->ID, "trial", true); 
							if ($out !== "") echo wpm_translate_string($out);
							else _e("unknown", "hatecrimes");
						?>
						</p>-->

						<p><strong><?php _e("Sentence", "hatecrimes")?>: </strong><br>
						<?php 
							$sentenceterms = get_the_terms($post->ID, "sentence_type");
							$out = "";
							foreach ($sentenceterms as $i => $term) {
								if ($i > 0) $out .= ", ";
								$out .= wpm_translate_string($term->name);
							}
							if ($out !== "") echo $out;
							else _e("unknown", "hatecrimes");
						?>
						</p>							
						<?php 
							$out = get_post_meta($post->ID, "sentence", true); 
							if ($out !== "" && strpos("NO", $out)) echo "<p>".wpm_translate_string($out)."</p>";
						?>

						<p><strong><?php _e("Delict", "hatecrimes")?>: </strong><br>
						<?php 
							$delictterms = get_the_terms($post->ID, "delict");
							$out = "";
							foreach ($delictterms as $i => $term) {
								if ($i > 0) $out .= ", ";
								$out .= wpm_translate_string($term->name);
							}
							if ($out !== "") echo $out;
							else _e("unknown", "hatecrimes");
						?>
						</p>
					</div><!-- .entry-custom -->

					<div class="entry-content">
						<?php 
							// videos
							$videos = get_post_meta($post->ID, "videos", true);
							if ($videos) {
								echo "<h3>Vídeos</h3><ul class='videos'>";
								$videos = explode("\n", $videos);
								foreach ($videos as $i=>$video) {
									if (trim($video) != "") {
										// strip url to get youtube id
										$video = str_replace("https://www.youtube.com/embed/", "", $video);
										$video = str_replace("https://www.youtube.com/watch?v=", "", $video);
										$video = str_replace('"', "'", $video);

										// separte url and title
										$arr = explode("___", $video);

										// no title, use url
										if (count($arr) == 1) {
											$arr[] = 'vídeo '.$i;
										}
						            	
						            	echo '<li>'.do_shortcode('[video_lightbox_youtube video_id="'.$arr[0].'" width="560" height="315" anchor="'.$arr[1].'"]').'</li>';
						            }
					            }
								echo "</ul>";
							}

							// fuentes
							$attachments = get_posts( array(
					            'post_type' => 'attachment',
					            'posts_per_page' => -1,
					            'post_parent' => $post->ID,
					            'order' => 'ASC',
					            'exclude' => get_post_thumbnail_id()
					        ));
					        if ( $attachments ) { ?>
								<h3><?php if (wpm_get_language() == 'ca') echo 'Fonts'; elseif (wpm_get_language() == 'es') echo 'Fuentes'; ?></h3>
								<ul class='fuentes'>
					            <?php foreach ($attachments as $attachment) {
					            	echo "<li><a target='_blank' href='".str_replace("/dev/", "/", wp_get_attachment_url($attachment->ID))."'>".wpm_translate_value($attachment->post_title)."</a></li>";
					            }
								echo "</ul>";
					        }
						?>

						</p>
					</div><!-- .entry-custom -->

					<div class="entry-footer">
						<?php if (wpm_get_language() == "ca"): ?>
							<?php foreach ($terms as $i => $term) { ?>
								<a href="<?php echo get_site_url().'/'.wpm_get_language().'/casos/?type='.$term->name; ?>" class="landing-btn">Altres casos de <?php echo $term->name; ?></a>
							<?php } ?>
							<a href="<?php echo get_site_url().'/'.wpm_get_language(); ?>/casos/" class="landing-btn">Altres crims d'odi</a>
							<br>
							<br>
							<a href="<?php echo esc_url(get_permalink($post->ID)); ?>?style=fitxa" class="landing-btn black">Descarrega la fitxa</a>
						<?php elseif (wpm_get_language() == "es"): ?>
							<?php foreach ($terms as $i => $term) { ?>
								<a href="<?php echo get_site_url().'/'.wpm_get_language().'/casos/?type='.$term->name; ?>" class="landing-btn">Otros casos de <?php echo $term->name; ?></a>
							<?php } ?>
							<a href="<?php echo get_site_url().'/'.wpm_get_language(); ?>/casos/" class="landing-btn">Otros crímenes de odio</a>
							<br>
							<br>
							<a href="<?php echo esc_url(get_permalink($post->ID)); ?>?style=fitxa" class="landing-btn black">Descarga la ficha</a>
						<?php endif; ?>
					</div>

				</div><!-- #post-## -->

				<hr>

				<?php comments_template( "", true ); ?>

			<?php endwhile; // end of the loop. ?>

			<?php cryout_after_content_hook(); ?>
			</div><!-- #content -->
	<?php //get_sidebar("right"); //nirvana_get_sidebar(); ?>
		</section><!-- #container -->

<?php get_footer(); ?>