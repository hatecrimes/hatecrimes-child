<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
  	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
  	wp_enqueue_style( 'fonts-style', get_stylesheet_directory_uri() . '/webfonts/fonts.css' );
    wp_enqueue_style( 'mobile-style', get_stylesheet_directory_uri() . '/style-mobile.css', array('nirvana-mobile') );
}

add_action('plugins_loaded', 'wan_load_textdomain');
function wan_load_textdomain() {
	load_plugin_textdomain( 'hatecrimes', false, dirname( plugin_basename(__FILE__) ) );
}

/* redirect after login */
function admin_default_page() {
  return ( '/' );
}
add_filter('login_redirect', 'admin_default_page');

/* desactivar default options */
//add_filter( 'radio-buttons-for-taxonomies-no-term-sentence_type', '__return_FALSE' );
//add_filter( 'radio-buttons-for-taxonomies-no-term-delict', '__return_FALSE' );

add_filter( 'gettext', 'change_rb4t_strings', 20, 3 );

function change_rb4t_strings( $translated_text, $text, $domain ) {

    if( 'radio-buttons-for-taxonomies' == $domain && 'No %s' == $translated_text ){
        $translated_text = __( 'Unknown', 'hatecrimes' );
    }

    return $translated_text;
}

/**
 * alternative template for visualizing single hatecrimes as fitxa
 */
add_filter('template_include', 'alt_hatecrime_single' );

function alt_hatecrime_single($template) {
  if( is_singular('hatecrime') ) {
    $alt = filter_input(INPUT_GET, 'style', FILTER_SANITIZE_STRING);
    if ( $alt === 'fitxa' ) $template = get_stylesheet_directory() . '/single-hatecrime-fitxa.php';
    else if ( $alt === 'image' ) $template = get_stylesheet_directory() . '/single-hatecrime-image.php';
  }
  return $template;
}

?>
